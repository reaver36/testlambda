NOTE: The access keys need to be from a user that has the roles of AWSLambdaFullAccess, IAMFullAccess, AmazonAPIGatewayAdministrator

## Creating a new lambda function
* Install the AWS cli.
* Add a named account in the local .aws/credentials
```
[claudia]
aws_access_key_id = YOUR_ACCESS_KEY
aws_secret_access_key = YOUR_ACCESS_SECRET
```
* Change the region in the package.json create script if using a different one
## Setting up build
* Set project to name to what ever you want
* In the Source section:
    * Select BitBucket as the source provider
    * Under Repository select Repository in my Butbucket account
    * Select the appropriate Bitbucket repository
    * Open additional configuration
    * Check the option Report build statuses to source provider when your builds start and finish
    * Check the option Rebuild every time a code change is pushed to this repository
    * enter master into the Branch filter (or whatever branch yout want)
* In the Environment section:
    * Select Managed image for the Environment image
    * Select Ubuntu as Operating system
    * Select Node.js as trhe Runtime
    * Select version 10.1.0 for the Runtime version
    * Select New service role for Service role
    * Open Additional configuration
    * In Environment variables add the following variables
        * REGION
        * AWS_ACCESS_KEY_ID
        * AWS_SECRET_ACCESS_KEY
    * Click the Create build project button

Currently the build process assumes the function has already been created using claudia.js
Build status will appear in the Commits page of the bitbucket repository page