const expect = require('chai').expect;
const context = require('aws-lambda-mock-context');
const index = require('../index');


describe("test lambda function", function(){
    it("some test", async function(){
        const ctx = context();

        let result = await index.handler({someVal: 5, someVal2: 2}, ctx);
        expect(result).to.have.property("statusCode");
        expect(result).to.have.property("body");
        expect(result.body).to.equal(7);
    });
});